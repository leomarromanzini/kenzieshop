import Header from "./components/Header";
import GLobalStyle from "./styles/global";
import Routes from "./routes";

function App() {
  return (
    <div className="App">
      <GLobalStyle />
      <Header />
      <Routes />
    </div>
  );
}

export default App;
