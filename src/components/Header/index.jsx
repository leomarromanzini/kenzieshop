import {
  AppBar,
  Badge,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import ShoppingCartSharpIcon from "@material-ui/icons/ShoppingCartSharp";
import { makeStyles } from "@material-ui/core/styles";

import { useHistory } from "react-router";

import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  appBar: {
    height: "70px",
    marginBottom: 40,
    backgroundColor: "#8377D1",
  },

  menuButton: {
    justifyContent: "flex-start",

    // marginRight: "88%",
  },
  cartButton: {
    justifyContent: "flex-end",
  },
}));

const Header = () => {
  const history = useHistory();
  const cart = useSelector((store) => store.cart);
  const classes = useStyles();

  const sendTo = (path) => {
    history.push(path);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <Button
            color="inherit"
            onClick={() => sendTo("/")}
            className={classes.menuButton}
          >
            KenzieShop
          </Button>

          <NavLink to="/cart">
            <IconButton>
              <Badge badgeContent={cart.length} color="secondary">
                <ShoppingCartSharpIcon style={{ color: "white" }} />
              </Badge>
              <Typography style={{ color: "white" }}> Carrinho</Typography>
            </IconButton>
          </NavLink>
          {/* <Button color="inherit" className={classes.cartButton}>
            Login
          </Button> */}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
