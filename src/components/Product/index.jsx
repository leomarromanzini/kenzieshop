import { Grid, makeStyles, Paper, Button } from "@material-ui/core";
import { useDispatch } from "react-redux";

import { addToCartThunk } from "../../store/modules/cart/thunk";
import formatValue from "../../utils/formatValue";

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: "white",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    minWidth: 300,
    width: "100%",
    height: 350,
  },
}));

const Product = ({ product, isRemovable }) => {
  const dispatch = useDispatch();

  const { name, price, image } = product;
  const classes = useStyles();
  return (
    <Grid item>
      <Paper className={classes.paper}>
        <div
          className="wrap"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            height: "60%",
          }}
        >
          <img alt={name} src={image}></img>
        </div>
        <span>{name}</span>
        <span>{formatValue(price)}</span>

        <Button
          color="secondary"
          variant="contained"
          onClick={() => dispatch(addToCartThunk(product))}
        >
          Adicionar item ao carrinho
        </Button>
      </Paper>
    </Grid>
  );
};

export default Product;
