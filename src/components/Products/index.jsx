import { useSelector } from "react-redux";
import Product from "../Product";
import { Grid } from "@material-ui/core";
const Products = () => {
  const products = useSelector((store) => store.products);

  return (
    <Grid
      container
      spacing={2}
      item
      xs={12}
      justify="space-around"
      alignItems="center"
      style={{ maxWidth: "100%", paddingRight: "10px", paddingLeft: "25px" }}
    >
      {products.map((product) => (
        <Product product={product} key={product.id} />
      ))}
    </Grid>
  );
};

export default Products;
