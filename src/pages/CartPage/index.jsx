import Cart from "../../components/Cart";
import { Button, Grid, Paper, Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import formatValue from "../../utils/formatValue";

const CartPage = () => {
  const cart = useSelector((store) => store.cart);
  const history = useHistory();

  const totalCart = formatValue(
    cart.reduce((acc, product) => product.price + acc, 0).toFixed(2)
  );

  if (!cart.length) {
    return (
      <Grid container>
        <Grid item xs={12}>
          <Paper
            style={{
              display: "flex",
              alignItems: "center",
              height: 260,
              flexDirection: "column",
            }}
          >
            <h1
              style={{
                textAlign: "center",
                paddingTop: 40,
                textTransform: "uppercase",
                fontSize: 26,
              }}
            >
              Sem produtos no carrinho, clique no botão abaixo para voltar
            </h1>
            <Button
              color="secondary"
              variant="contained"
              style={{ marginTop: 50 }}
              onClick={() => history.push("/")}
            >
              Voltar
            </Button>
          </Paper>
        </Grid>
      </Grid>
    );
  }

  return (
    <div className="cartContainer">
      <div>
        <Grid>
          <Paper className="paperCart">
            <Typography style={{ textAlign: "center" }} variant="h4">
              Resumo do pedido
            </Typography>
            <div className="textWrap">
              <Typography variant="h6">{cart.length} Produtos</Typography>
              <Typography variant="h6">Total - {totalCart}</Typography>
            </div>
            <Button color="secondary" variant="contained">
              Finalizar compra
            </Button>
          </Paper>
        </Grid>
      </div>
      <Grid
        container
        spacing={2}
        xs={12}
        justify="center"
        alignItems="center"
        style={{ maxWidth: "100%", paddingRight: 100, marginRight: 20 }}
      >
        {cart.map((product) => (
          <Cart product={product} key={product.name} isRemovable />
        ))}
      </Grid>
    </div>
  );
};

export default CartPage;
