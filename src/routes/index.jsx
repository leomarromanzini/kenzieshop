import { Switch, Route } from "react-router-dom";
import CartPage from "../pages/CartPage";
import Home from "../pages/Home";
import NotFound from "../pages/NotFound";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home></Home>
      </Route>
      <Route path="/cart">
        <CartPage />
      </Route>
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
};

export default Routes;
