import { ADD_CART, REMOVE_CART } from "./actionsTypes";

const defaultState = JSON.parse(localStorage.getItem("cart")) || [];

const cartReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_CART:
      return [...state];

    case REMOVE_CART:
      const { list } = action;

      return list;

    default:
      return state;
  }
};

export default cartReducer;
