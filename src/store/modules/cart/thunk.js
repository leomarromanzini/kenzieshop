import { addToCart, removeFromCart } from "./actions";

export const addToCartThunk = (product) => (dispatch, getStore) => {
  const { cart } = getStore();
  cart.push(product);
  localStorage.setItem("cart", JSON.stringify(cart));

  dispatch(addToCart(product));
};

export const removeFromCartThunk = (id) => (dispatch, getStore) => {
  const { cart } = getStore();

  const list = cart.filter((product) => product.id !== id);

  localStorage.setItem("cart", JSON.stringify(list));
  dispatch(removeFromCart(list));
};
