export const products = [
  {
    id: 1,
    name: "Echo Dot",
    price: 279.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/714Rq4k05UL._AC_SY355_.jpg",
  },
  {
    id: 2,
    name: "Kindle Paperwhite",
    price: 499.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/61ldUg%2BPqQL._AC_SX425_.jpg",
  },
  {
    id: 3,
    name: "Fire TV Stick",
    price: 360.05,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/51degAt5CFL._AC_SY450_.jpg",
  },
  {
    id: 4,
    name: 'Apple MacBook Pro 13"',
    price: 9875.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/71an9eiBxpL._AC_SX522_.jpg",
  },
  {
    id: 5,
    name: "Apple Iphone 12 (128GB Preto)",
    price: 6390.0,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/71fVoqRC0wL._AC_SX679_.jpg",
  },
];
