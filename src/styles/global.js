import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;

    }

    :root {
       
        --black: #0c0d0d;
        
    }

    body{
        background: #8EF9F3;
        color: var(--black)
    }

    body,input, button {
        font-family: "PT Serif", serif;
        font-size: 1rem;
    }

    h1,h2,h3,h4,h5,h6 {
        font-family: "Roboto Mono", monospace;
        font-weight: 700;
    }

    button {
        cursor: pointer;
    }

    a {
        text-decoration: none;
    }

    img{
        width: 140px;
        
    }

    .app{
        display:flex;
        align-items: center;
        justify-content: center;
    }

    .cartContainer{
        display: flex; 
        flex-direction: row-reverse;
    }

    .textWrap{
              display: flex;
              justify-content: space-around;
              padding: 20px;
    }

    .paperCart{
    min-width: 500px;
    margin: 10px;
    display: flex;
    flex-direction: column;
    padding: 30px 10px;
  }

    @media screen and (max-width: 700px){
        .cartContainer{
            flex-direction: column;
            justify-content: center;
            align-items: center;
    }
        
    .textWrap{
              display: flex;
              flex-direction: column;
              justify-content: center;
              align-items: center;
              padding: 20px;
    }

    .paperCart{
    min-width: 450px;
    }
    }

    @media screen and (max-width: 500px){
        .cartContainer{
            flex-direction: column;
            justify-content: center;
            align-items: center;
    }
        
    .textWrap{
              display: flex;
              flex-direction: column;
              justify-content: center;
              align-items: center;
              padding: 20px;
    }

    .paperCart{
    min-width: 300px;
    }
    }
`;
